import os
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

class WeekTraffic():
    def __init__(self, data_dir = './ready_data/traffic'):
        sns.set(font_scale = 1.5)
        self.days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

        self.observations = pd.read_csv(os.path.join(data_dir, 'observations.csv'), index_col=0)
        self.daysOfWeek = dict()
        for day in self.days:
            self.daysOfWeek[day] = self.types = pd.read_csv(os.path.join(data_dir, '%s.csv' % day), index_col=0, dtype = {'Brigade': str})
        
        self.weekdays = pd.read_csv(os.path.join(data_dir, 'weekdays.csv'), index_col=0)
        self.weekends = pd.read_csv(os.path.join(data_dir, 'weekends.csv'), index_col=0)

    def plot_observations(self):
        fig = plt.figure(figsize = (12,8))
        y_pos = np.arange(len(self.days))

        bars = plt.bar(np.arange(7), self.observations.Count.tolist(), align = 'center', alpha = 0.5)
        plt.xticks(y_pos, self.days)
        plt.xlabel('Day of week')
        plt.ylabel('Observations')

        for bar, count in zip(bars, self.observations.Count):
            height = bar.get_height()
            plt.text(bar.get_x() + bar.get_width() / 2, height, str(count), ha='center', va='top')
        return fig

    def plot_days_of_week(self):
        fig, ax = plt.subplots(figsize = (12,8))

        for day in self.days:
            ax.plot(self.daysOfWeek[day].Hour, self.daysOfWeek[day].Count, label = day)

        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, self.days)

        labels = ["%d:00" % (hour, ) for hour in range(0, 24, 2)]
        ax.set_xticklabels(labels)
        ax.set_xticks(np.arange(0, 24, 2))
        plt.xlabel('Time')
        plt.ylabel('Observations')
        return fig

    def plot_weekdays_weekends(self):
        fig, ax = plt.subplots(figsize = (12,8))
        weekdays = self.days[0:5]
        weekends = self.days[5:7]	

        ax.plot(self.weekdays.Hour, self.weekdays.Count, label = 'Weekdays')
        ax.plot(self.weekends.Hour, self.weekends.Count, label = 'Weekends')

        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, ('Weekdays', 'Weekends'))

        labels = ["%d:00" % (hour, ) for hour in range(0, 24, 2)]
        ax.set_xticklabels(labels)
        ax.set_xticks(np.arange(0, 24, 2))
        plt.xlabel('Time')
        plt.ylabel('Observations')
        return fig
