import pandas as pd
import matplotlib.pyplot as plt

class StopData():
    def __init__(self):
        self.counts = pd.read_csv('ready_data/stop_counts.csv')
        self.counts_no_ends = pd.read_csv('ready_data/stop_counts_no_ends.csv')
        
    def plot(self, ends = False):
        if ends:
            data = self.counts
        else:
            data = self.counts_no_ends
        plt.figure(figsize=(15, 15))
        cm = plt.get_cmap('autumn')
        max_stop = data['count'].max()
        for idx, stop in data.iterrows():
            scaled = stop['count'] / max_stop
            plt.plot(stop['stop_lon'], stop['stop_lat'], 'o', markersize=scaled * 30 + 10, color = cm(scaled))
            plt.annotate(stop['stop_name'], [stop['stop_lon'], stop['stop_lat']])