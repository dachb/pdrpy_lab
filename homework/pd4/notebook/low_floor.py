import os
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

class LowFloor():
    def __init__(self, data_dir = './ready_data/low_floor'):
        sns.set(font_scale = 1.5)
        self.days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

        self.byLines = pd.read_csv(os.path.join(data_dir, 'by_lines.csv'), index_col=0)
        self.byDayOfWeek = pd.read_csv(os.path.join(data_dir, 'by_day_of_week.csv'), index_col=0)
        self.ratioWeekdays = pd.read_csv(os.path.join(data_dir, 'ratio_at_weekdays.csv'), index_col=0)
        self.ratioWeekends = pd.read_csv(os.path.join(data_dir, 'ratio_at_weekends.csv'), index_col=0)

    def plot_by_lines(self):
        fig, ax = plt.subplots(figsize = (12,8))
        rects1 = ax.bar(np.arange(len(self.byLines)), self.byLines.Ratio,
		       color = 'red', alpha = 0.55)

        ax.set_xticklabels(self.byLines.index)
        ax.set_xticks(np.arange(len(self.byLines.index)))
        plt.xlabel('Line')
        plt.ylabel('Low-floor ratio')
        return fig

    def plot_by_days_of_week(self):
        fig, ax = plt.subplots(figsize = (12,8))
        rects1 = ax.bar(np.arange(len(self.byDayOfWeek)), self.byDayOfWeek.Ratio,
		       color = 'green', alpha = 0.5)
	
        ax.set_xticklabels(self.byDayOfWeek.index)
        ax.set_xticks(np.arange(len(self.byDayOfWeek.index)))
        plt.xlabel('Day of week')
        plt.ylabel('Low-floor ratio')
        return fig

    def plot_ratio_weekdays_weekends(self):
        fig, ax = plt.subplots(figsize = (12,8))
        weekdays = self.days[0:5]
        weekends = self.days[5:7]	

        fig, ax = plt.subplots(figsize = (12,8))
        ax.plot(self.ratioWeekdays.Hour, self.ratioWeekdays.Count, label = 'Weekdays')
        ax.plot(self.ratioWeekends.Hour, self.ratioWeekends.Count, label = 'Weekends')
	    
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, ('Weekdays', 'Weekends'))
	    
        labels = ["%d:00" % (hour, ) for hour in range(0, 24, 2)]
        ax.set_xticklabels(labels)
        ax.set_xticks(np.arange(0, 24, 2))
        plt.xlabel('Time')
        plt.ylabel('Low-Floor ratio')
        return fig
