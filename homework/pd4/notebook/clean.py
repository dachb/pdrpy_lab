import pandas as pd
import json
import datetime
import os

def clean_file(json_file):
    with open(json_file, 'r') as open_file:
        try:
            contents = json.load(open_file)
        except json.decoder.JSONDecodeError:
            return pd.DataFrame()
    if 'result' not in contents or not isinstance(contents['result'], list):
        return None
    file_frame = pd.io.json.json_normalize(contents['result'])
    if (len(file_frame) <= 0):
        return file_frame
    file_frame.loc[:,"Brigade"] = file_frame.loc[:,"Brigade"].apply(lambda x: x.strip())
    file_frame.loc[:,"FirstLine"] = file_frame.loc[:,"FirstLine"].apply(lambda x: x.strip())
    file_frame.loc[:,"Lines"] = file_frame.loc[:,"Lines"].apply(lambda x: ','.join(x.strip().split()))
    file_frame.loc[:, "Time"] = file_frame.loc[:, "Time"].apply(lambda x: datetime.datetime.strptime(x, "%Y-%m-%dT%H:%M:%S"))
    file_frame.loc[:, "Year"] = file_frame.loc[:, "Time"].apply(lambda x: x.year)
    file_frame.loc[:, "Month"] = file_frame.loc[:, "Time"].apply(lambda x: x.month)
    file_frame.loc[:, "Day"] = file_frame.loc[:, "Time"].apply(lambda x: x.day)
    file_frame.loc[:, "Hour"] = file_frame.loc[:, "Time"].apply(lambda x: x.hour)
    file_frame.loc[:, "Minute"] = file_frame.loc[:, "Time"].apply(lambda x: x.minute)
    file_frame.loc[:, "Second"] = file_frame.loc[:, "Time"].apply(lambda x: x.second)
    idx = (file_frame.loc[:,"Lon"] >= 20.8) & (file_frame.loc[:,"Lon"] <= 21.2) & (file_frame.loc[:,"Lat"] >= 52.1) & (file_frame.loc[:,"Lat"] <= 52.5)
    return file_frame.loc[idx, :]

def clean():
    file_frames = []
    for root, dirs, files in os.walk('trams'):
        for filename in files:
            fullname = os.path.join(root, filename)
            print('Cleaning {}'.format(fullname))
            file_frame = clean_file(os.path.join(fullname))
            file_frames.append(file_frame)
    frame = pd.concat(file_frames)
    frame.reset_index()
    frame.to_csv('clean.csv')

if __name__ == "__main__":
    clean()
