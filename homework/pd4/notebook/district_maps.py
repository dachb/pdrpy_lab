import pandas as pd
import matplotlib.pyplot as plt
import shapely.geometry as geom
import descartes
import os
import json
import seaborn as sns
import calendar
import matplotlib.animation as anim
from matplotlib import rc

from matplotlib.collections import PatchCollection

class DistrictMaps:
    def __init__(self, csv_path = './ready_data/trams_in_districts_per_hour.csv'):
        self.base_data = pd.read_csv(csv_path)
        self.shapes = self.read_boundaries()
        self.outline = self.shapes.pop('Warsaw')
        rc('animation', html='html5')
        
    def read_boundaries(self):
        plt.close('all')
        shapes = {}
        for root, dirs, files in os.walk('boundaries/'):
            for file in files:
                with open(os.path.join(root, file), 'r') as geofile:
                    geojson = json.load(geofile)
                name = os.path.splitext(file)[0]
                first = geojson["geometries"][0]
                shapes[name] = geom.shape(first).geoms[0]
        return shapes
    
    def plot_overall(self):
        plt.close('all')
        fig = plt.figure(figsize=(15, 15), facecolor='black')
        ax = fig.add_subplot(111)
        self.plot_series(ax, self.base_data.sum(), labels='count')
        fig.suptitle('Liczba zarejestrowanych położeń tramwajów w danej dzielnicy', fontsize=20, color='white')
        return fig
    
    def plot_workdays_weekends(self):
        plt.close('all')
        fig = plt.figure(figsize=(24, 12), facecolor='black')
        ax1 = fig.add_subplot(121)
        self.plot_series(ax1, self.base_data[self.base_data.loc[:, 'Weekday'] <= 4].sum(), labels='percent')
        ax1.set_title('Dni robocze', color='white', fontsize=18)
        ax2 = fig.add_subplot(122)
        self.plot_series(ax2, self.base_data[self.base_data.loc[:, 'Weekday'] >= 5].sum(), labels='percent')
        ax2.set_title('Weekend', color='white', fontsize=18)
        fig.subplots_adjust(wspace=0, hspace=0)
        fig.suptitle('Mapa ciepła - weekendy kontra dni robocze', fontsize=20, color='white')
        return fig
    
    def plot_workdays_weekends_per_day(self):
        plt.close('all')
        fig = plt.figure(figsize=(24, 12), facecolor='black')
        ax1 = fig.add_subplot(121)
        self.plot_series(ax1, self.base_data[self.base_data.loc[:, 'Weekday'] <= 4].sum() / 5, labels='count')
        ax1.set_title('Dni robocze', color='white', fontsize=18)
        ax2 = fig.add_subplot(122)
        self.plot_series(ax2, self.base_data[self.base_data.loc[:, 'Weekday'] >= 5].sum() / 2, labels='count')
        ax2.set_title('Weekend', color='white', fontsize=18)
        fig.suptitle('Mapa ciepła - weekendy kontra dni robocze', fontsize=20, color='white')
        fig.subplots_adjust(wspace=0, hspace=0)
        return fig
    
    def plot_hour_heatmap(self):
        plt.close('all')
        clean = self.base_data.drop(['Weekday', 'Rembertów', 'Ursus', 'Ursynów', 'Wawer', 'Wilanów'], 1)
        by_hour = clean.groupby('Hour').sum()
        fig = plt.figure(figsize=(20, 15))
        sns.set(font_scale=1.6)
        ax = sns.heatmap(by_hour, cmap=plt.get_cmap('viridis'))
        plt.xticks(rotation=90)
        plt.yticks(rotation=0)
        return ax.get_figure()
    
    def interactive_plot(self, **kwargs):
        plt.close('all')
        fig = plt.figure(figsize=(20, 20), facecolor='black')
        ax = fig.add_subplot(111)
        data = self.base_data
        labels = []
        if 'weekday' in kwargs:
            data = data[data.loc[:, 'Weekday'] == kwargs['weekday']]
            labels.append(calendar.day_name[kwargs['weekday']])
        else:
            labels.append('Whole week')
        if 'hour' in kwargs:
            data = data[data.loc[:, 'Hour'] == kwargs['hour']]
            labels.append('{0:02}:00-{0:02}:59'.format(kwargs['hour']))
        else:
            labels.append('all day')
        text = ''
        try:
            text = kwargs['labels']
        except KeyError:
            pass
        self.plot_series(ax, data.sum(), labels=text)
        fig.suptitle(', '.join(labels), color='white')
        return fig
    
    def animation(self):
        fig = plt.figure(figsize=(10, 10), facecolor='black')
        ax = fig.add_subplot(111)

        def animate(i):
            maxcount = self.base_data.max().max()
            row = self.base_data.loc[i, :]
            self.plot_series(ax, row, max=maxcount)
            fig.suptitle('{0}, {1:02}:00-{1:02}:59'.format(calendar.day_name[row['Weekday']], row['Hour']), fontsize=20)

        return anim.FuncAnimation(fig, animate, len(self.base_data), interval=500)
    
    def plot_series(self, ax, series, **kwargs):
        plt.close('all')
        try:
            series = series.drop(['Weekday', 'Hour'])
        except ValueError:
            pass

        minx, miny, maxx, maxy = self.outline.bounds
        w, h = maxx - minx, maxy - miny
        ax.set_xlim(minx, maxx)
        ax.set_ylim(miny, maxy)
        ax.set_aspect(1.5)
        ax.set_facecolor('black')
        ax.tick_params(labelbottom='off', labelleft='off')
        ax.grid(color='black')

        cm = plt.get_cmap('viridis')

        patches = []
        for name, shape in self.shapes.items():
            try:
                count = series.loc[name]
            except KeyError:
                count = 0
            if 'max' in kwargs:
                series_max = kwargs['max']
            else:
                series_max = series.max()
            colour = cm(0.85 * count / series_max)
            patches.append(descartes.PolygonPatch(shape, fc = colour, ec = "#666666", lw = 0.2))
            x, y = shape.centroid.coords[0]
            if 'labels' in kwargs:
                if kwargs['labels'] == 'percent':
                    text = '{0}\n{1:.2f}%'.format(name, count / series.sum() * 100)
                elif kwargs['labels'] == 'count':
                    text = '{0}\n{1}'.format(name, count)
                else:
                    text = ''
                ax.text(x, y, text, ha='center', va='center', size=16, color='white')
        ax.add_collection(PatchCollection(patches, match_original=True))
        return ax