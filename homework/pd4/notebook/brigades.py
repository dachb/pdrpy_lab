import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os
import district_maps as maps

class BrigadeData:
    def __init__(self, data_dir = './ready_data/'):
        sns.set(font_scale=2)
        self.types = pd.read_csv(os.path.join(data_dir, 'brigade_types.csv'), index_col=0)
        self.per_district = pd.read_csv(os.path.join(data_dir, 'brigade_types_per_district.csv'), index_col=0)
        self.per_line = pd.read_csv(os.path.join(data_dir, 'brigade_types_per_line.csv'), index_col=0)
        self.per_hour = pd.read_csv(os.path.join(data_dir, 'brigade_types_per_hour.csv'), index_col=0)
        self.lines_by_hour = pd.read_csv(os.path.join(data_dir, 'lines_by_hour.csv'), index_col=0)
        
    def prepare_data(self, data, typename = None):
        if typename in ['Brygady zwykłe', 'Brygady szczytowe', 'Wtyczki szczytowe']:
            data = data.loc[:, typename]
        return data
    
    def plot_types(self, typename = ''):
        plt.close('all')
        data = self.prepare_data(self.types, typename)
        ax = data.plot.bar(stacked=True, figsize=(16, 10))
        plt.title(': '.join(['Skład poszczególnych brygad', typename]))
        plt.xlabel('Numer brygady')
        plt.ylabel('Liczba rekordów')
        return ax.get_figure()
    
    def plot_per_line(self, typename = ''):
        plt.close('all')
        data = self.prepare_data(self.per_line, typename)
        ax = data.plot.bar(stacked=True, figsize=(16, 10))
        plt.title(': '.join(['Skład brygad obsługujących dane linie', typename]))
        plt.xlabel('Numer linii')
        plt.ylabel('Liczba rekordów')
        return ax.get_figure()
    
    def plot_per_hour(self, typename = ''):
        plt.close('all')
        data = self.prepare_data(self.per_hour, typename)
        ax = data.plot.bar(stacked=True, figsize=(16, 10))
        plt.title(': '.join(['Typ brygady a godzina', typename]))
        plt.xlabel('Numer linii')
        plt.ylabel('Liczba rekordów')
        return ax.get_figure()
    
    def plot_per_district(self, typename):
        if typename not in ['Brygady zwykłe', 'Brygady szczytowe', 'Wtyczki szczytowe']:
            raise ValueError("Type name must be one of ['Brygady zwykłe', 'Brygady szczytowe', 'Wtyczki szczytowe']")
        plt.close('all')
        fig = plt.figure(figsize = (15, 15), facecolor='black')
        ax = fig.add_subplot(111)
        data = self.prepare_data(self.per_district, typename)
        mapper = maps.DistrictMaps()
        mapper.plot_series(ax, data, labels='percent')
        fig.suptitle('Rozkład geograficzny brygad - {0}'.format(typename), fontsize=20, color='white')
        return fig
    
    def lines_per_hour_heatmap(self):
        plt.close('all')
        fig = plt.figure(figsize=(20, 20))
        ax = sns.heatmap(self.lines_by_hour, cmap=plt.get_cmap('viridis'))
        plt.xlabel('Numer linii')
        plt.ylabel('Godzina')
        return ax.get_figure()