import requests
import time
import datetime
import os

def save(text):
    now = datetime.datetime.now()
    dirname = os.path.join(str(now.date()))
    filename = now.time().strftime('%H-%M-%S') + ".json"
    if not os.path.isdir(dirname):
        os.mkdir(dirname)
    fullname = os.path.join(dirname, filename)
    with open(fullname, 'w') as f:
        f.write(text)
    print('Saved file {}'.format(fullname))

def scrape_trams(api_key):
    url = 'https://api.um.warszawa.pl/api/action/wsstore_get/?id=c7238cfe-8b1f-4c38-bb4a-de386db7e776&apikey={}'.format(api_key)
    os.chdir("/home/dachb/trams")
    try:
        while True:
            request = requests.get(url)
            save(request.text)
            time.sleep(35)
    except (KeyboardInterrupt, SystemExit):
        raise

if __name__ == '__main__':
    scrape_trams('43c40b1f-3023-4131-a962-85d8ce242e21')
