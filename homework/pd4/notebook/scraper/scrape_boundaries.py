import requests
import os

def scrape():
    base_url = 'http://polygons.openstreetmap.fr/get_geojson.py?id={}&params=0'
    districts = {
            'Warsaw': 336074,
            'Bemowo': 2531595,
            'Białołęka': 2556629,
            'Bielany': 2531981,
            'Mokotów': 2536106,
            'Ochota': 2534033,
            'Praga-Południe': 2544288,
            'Praga-Północ': 2553341,
            'Rembertów': 2579781,
            'Śródmieście': 2534036,
            'Targówek': 2556630,
            'Ursus': 2534034,
            'Ursynów': 2536107,
            'Wawer': 2579782,
            'Wesoła': 2579783,
            'Wilanów': 2536108,
            'Włochy': 2534035,
            'Wola': 2531982,
            'Żoliborz': 2531983
    }
    for name, shape_id in districts.items():
        request = requests.get(base_url.format(shape_id))
        with open(name, 'w') as out_file:
            out_file.write(request.text)

if __name__ == '__main__':
    scrape()
