#include <Rcpp.h>
#include <algorithm>
#include <vector>
#include <cmath>

// [[Rcpp::plugins(cpp11)]]

void checkArguments(const Rcpp::NumericMatrix& trainingSet,
                    const Rcpp::NumericMatrix& testingSet,
                    const long& k,
                    const double& p);

unsigned int assignLabel(const Rcpp::NumericVector& row,
                         const Rcpp::NumericMatrix& testingSet,
                         const Rcpp::NumericVector& labels,
                         long k,
                         double p,
                         bool avgLabels);

unsigned int mode(std::vector<unsigned long>::iterator begin,
                  std::vector<unsigned long>::iterator end,
                  const Rcpp::NumericVector& labels);

/**
 * \brief Performs a classification with the given data using the k nearest
 * neighbors algorithm.
 * 
 * \param trainingSet The set of training data to base classification on.
 * \param labels      The labels assigned to each observation from the
 *                    training set.
 * \param testingSet  The dataset to classify.
 * \param k           Parameter of the k-NN algorithm. Indicates how many
 *                    of the closest observations to consider when
 *                    classifying.
 * \param p           Parameter of the k-NN algorithm. Indicates that the
 *                    p-th Minkowski norm will be used when calculating
 *                    distances between observations.
 * \param avgLabels   If set to false, the label selected will be the
 *                    mode of the nearest k labels; otherwise, the label will
 *                    be calculated by treating the labels as numbers and
 *                    rounding the mean of the numbers to the nearest number.
 */

// [[Rcpp::export]]

RcppExport SEXP knn(Rcpp::NumericMatrix trainingSet,
                    Rcpp::NumericVector labelNumbers,
                    Rcpp::NumericMatrix testingSet,
                    long k,
                    double p = 2,
                    bool avgLabels = false)
{
  Rcpp::CharacterVector labels(labelNumbers.attr("levels"));
  checkArguments(trainingSet, testingSet, k, p);
  
  std::vector<unsigned int> assignedLabels(testingSet.nrow());
  for (unsigned long i = 0; i < testingSet.nrow(); ++i) {
    assignedLabels[i] = assignLabel(testingSet.row(i), trainingSet, labelNumbers, k, p, avgLabels);
  }
  
  auto result = Rcpp::IntegerVector(assignedLabels.begin(), assignedLabels.end());
  result.attr("levels") = labels;
  result.attr("class") = "factor";
  return result;
}

/**
 * \brief Checks whether the arguments satisfy the necessary conditions for
 * k-NN algorithm input.
 * 
 * \param trainingSet The set of training data to base classification on.
 * \param testingSet  The dataset to classify.
 * \param k           Parameter of the k-NN algorithm. Indicates how many
 *                    of the closest observations to consider when
 *                    classifying.
 * \param p           Parameter of the k-NN algorithm. Indicates that the
 *                    p-th Minkowski norm will be used when calculating
 *                    distances between observations.
 */
void checkArguments(const Rcpp::NumericMatrix& trainingSet,
                    const Rcpp::NumericMatrix& testingSet,
                    const long& k,
                    const double& p)
{
  if (trainingSet.ncol() != testingSet.ncol())
    Rcpp::stop("The training and testing sets have a different number of features");
  if (k < 1)
    Rcpp::stop("Invalid value of k (must be more than one)");
  if (k > trainingSet.nrow())
    Rcpp::stop("Invalid value of k (must be less or equal to number of rows in training set)");
  if (p < 1)
    Rcpp::stop("Invalid value of k (must be more than one)");
}

/**
 * \brief Assigns one of the labels to the supplied observation row.
 * 
 * \param row         The row of the testing set to classify.
 * \param trainingSet The training data matrix.
 * \param labels      The labels for the training data.
 * \param k           Indicates which k nearest neighbors should be taken
 *                    into account.
 * \param p           Indicates which p-th norm is to be used.
 * \param avgLabels   If set to false, the label selected will be the
 *                    mode of the nearest k labels; otherwise, the label will
 *                    be calculated by treating the labels as numbers and
 *                    rounding the mean of the numbers to the nearest number.
 */
unsigned int assignLabel(const Rcpp::NumericVector& row,
                         const Rcpp::NumericMatrix& trainingSet,
                         const Rcpp::NumericVector& labels,
                         long k,
                         double p,
                         bool avgLabels)
{
  std::vector<double> distances(trainingSet.nrow());
  std::vector<unsigned long> indices(trainingSet.nrow());
  std::iota(indices.begin(), indices.end(), 0);
  for (unsigned long i = 0; i < trainingSet.nrow(); ++i) {
    Rcpp::NumericVector diff = abs(trainingSet.row(i) - row);
    if (std::isinf(p)) {
      distances[i] = max(diff);
    } else {
      double underRoot = sum(pow(diff, p));
      distances[i] = pow(underRoot, 1/p);
    }
  }
  std::sort(indices.begin(), indices.end(),
            [&distances](const unsigned long& i1, const unsigned long& i2) -> bool
              { return distances[i1] < distances[i2]; }
            );
  if (avgLabels) {
    float label = 0.0f;
    for (auto it = indices.begin(); it < indices.begin() + k; ++it) {
      label += labels[*it];
    }
    return std::round(label / k);
  }
  return mode(indices.begin(), indices.begin() + k, labels);
}

/**
 * \brief Calculates the mode (most popular value) from the labels,
 * which are specified by the indices supplied as an iterator range.
 * 
 * \param begin  The starting iterator.
 * \param end    The ending iterator.
 * \param labels The vector of labels.
 */
unsigned int mode(std::vector<unsigned long>::iterator begin,
                  std::vector<unsigned long>::iterator end,
                  const Rcpp::NumericVector& labels)
{
  std::unordered_map<unsigned int, unsigned long> counts;
  for (auto it = begin; it < end; ++it) {
    ++counts[labels[*it]];
  }
  std::multimap<unsigned long, unsigned int, std::greater<unsigned long>> sorted;
  for (auto pair : counts) {
    sorted.insert(std::make_pair(pair.second, pair.first));
  }
  auto max_end = sorted.upper_bound(sorted.begin()->first);
  auto count = std::distance(sorted.begin(), max_end);
  int idx = Rcpp::runif(1)[0] * count;
  auto result = sorted.begin();
  std::advance(result, idx);
  return result->second;
}